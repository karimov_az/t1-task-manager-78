package ru.t1.karimov.tm.api;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import ru.t1.karimov.tm.model.Project;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.Collection;

@WebService
public interface IProjectRestEndpoint {

    @WebMethod
    @PostMapping("/delete")
    void delete(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

    @PostMapping("delete/{id}")
    @WebMethod(operationName = "deleteById")
    void delete(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @GetMapping("/findAll")
    Collection<Project> findAll();

    @Nullable
    @WebMethod
    @GetMapping("findById/{id}")
    Project findById(
            @WebParam(name = "id")
            @PathVariable("id") String id
    );

    @NotNull
    @WebMethod
    @PostMapping("/save")
    Project save(
            @WebParam(name = "project", partName = "project")
            @RequestBody Project project
    );

}
